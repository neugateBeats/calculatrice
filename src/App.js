
// import './calculatrice.css'; 
import Calculator from './components/Calculator';
import React, { useState } from 'react';


function App() {
  return (
    <div className="App" data-testid="app-component">
      <Calculator />
    </div>
  );
}

export default App;
