
import React, { useState } from "react";
import Input from "./Input";
// import Button from "./Button";
import Result from "./Result";
import Button from 'react-bootstrap/Button';


// Composant calculatrice
function Calculator() {
    const [number1, setNumber1] = useState(0);
    const [number2, setNumber2] = useState(0);
    const [operator, setOperator] = useState("");
    const [result, setResult] = useState(0);
  
    // Gestion des opérations
    const handleOperation = (op) => {
      setOperator(op);
    };
  
    // Gestion des changements dans les inputs
    const handleInputChange = (name, value) => {
      if (name === "number1") {
        setNumber1(Number(value));
      } else if (name === "number2") {
        setNumber2(Number(value));
      }
    };
  
    // Calcul du résultat
    const calculateResult = () => {
      let res = 0;
      switch (operator) {
        case "+":
          res = number1 + number2;
          break;
        case "-":
          res = number1 - number2;
          break;
        case "*":
          res = number1 * number2;
          break;
        case "/":
          res = number1 / number2;
          break;
        default:
          break;
      }
      setResult(res);
    };
  
      // Effacement des données
  const clearData = () => {
    setNumber1(0);
    setNumber2(0);
    setOperator("");
    setResult(0);
  };
  return (
    <div className="container mt-5 ">
      <div className="row justify-content-center ">
        <div className="col-md-4 mt-5">
          <div className="border p-3 bg-danger">
            <Input
              label="Number 1:"
              name="number1"
              value={number1}
              onChange={(value) => handleInputChange("number1", value)}
            />
            <Input
              label="Number 2:"
              name="number2"
              value={number2}
              onChange={(value) => handleInputChange("number2", value)}
            />
            <div className="d-flex justify-content-between mb-3">
              <Button variant="primary" onClick={() => handleOperation("+")}>
                +
              </Button>
              <Button variant="primary" onClick={() => handleOperation("-")}>
                -
              </Button>
              <Button variant="primary" onClick={() => handleOperation("*")}>
                *
              </Button>
              <Button variant="primary" onClick={() => handleOperation("/")}>
                /
              </Button>
              <Button variant="light" onClick={clearData}>
                C
              </Button>
            </div>
  
            <button className="btn btn-primary w-100 " onClick={calculateResult}>
              Calculate
            </button>
            <Result variant="ml-5" value={result} />
          </div>
        </div>
      </div>
    </div>
  );
  

  }
  
  export default Calculator;