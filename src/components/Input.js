import React, { useState } from "react";



// Composant input
function Input(props) {
    return (
      <div className="Input">
        <label htmlFor={props.name}>{props.label}</label>
        <input
          type="number"
          id={props.name}
          name={props.name}
          value={props.value}
          onChange={(e) => props.onChange(e.target.value)}
        />
      </div>
    );
  };
  export default Input; 