import React from "react";


function Button(props) {
  return (
    <button className="Button" onClick={() => props.onClick(props.operation)}>
      {props.text}
    </button>
  );
}
export default Button;