import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Input from "../components/Input";

test("test du composant Input", () => {
  // Fonction de gestion du changement de la valeur
  const handleChange = jest.fn();

  // Rendu du composant Input avec des valeurs fictives
  render(
    <Input
      name="test-input"
      label="Test Input"
      value={10}
      onChange={handleChange}
    />
  );

  // Vérification de l'affichage du label
  const labelElement = screen.getByText("Test Input");
  expect(labelElement).toBeInTheDocument();

  // Vérification de la valeur initiale de l'input
  const inputElement = screen.getByRole("spinbutton");
  expect(inputElement).toHaveValue(10);

  // Simulation d'un changement de valeur dans l'input
  fireEvent.change(inputElement, { target: { value: "20" } });

  // Vérification que la fonction de gestion du changement a été appelée avec la nouvelle valeur
  expect(handleChange).toHaveBeenCalledWith("20");
});
