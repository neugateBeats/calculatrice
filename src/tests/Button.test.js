import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Button from "../components/button";

// test sur le button
test("test du bouton d'addition", () => {
  // Mock de la fonction de gestion du clic
  const handleClick = jest.fn();

  // Rendu du composant Button
  render(<Button text="addition" onClick={handleClick} operation="+" />);

  // Vérification de l'affichage du texte du bouton
  const buttonElement = screen.getByText(/addition/i);
  expect(buttonElement).toBeInTheDocument();

  // Simulation d'un clic sur le bouton
  fireEvent.click(buttonElement);

  // Vérification que la fonction de gestion du clic a été appelée avec l'opération "+"
  expect(handleClick).toHaveBeenCalledWith("+");
});











// import {render, screen} from '@testing-library/react'
// import Button from '../components/button'

// // test sur le button
// test('texte bouton addition', () => {
//  render(<Button text="addition" />)
//  const linkElement = screen.getByText(/addition/i)
//  expect(linkElement).toBeInTheDocument();
// });












// import React from "react";
// import { shallow } from "enzyme";
// import Button from "../components/button";

// describe("Button", () => {
//   const onClick = jest.fn();
//   const wrapper = shallow(
//     <Button text="+" operation="+" onClick={onClick} />
//   );

//   it("renders a button with the text passed in props", () => {
//     expect(wrapper.text()).toBe("+");
//   });

//   it("calls onClick when the button is clicked", () => {
//     wrapper.simulate("click");
//     expect(onClick).toHaveBeenCalledTimes(1);
//     expect(onClick).toHaveBeenCalledWith("+");
//   });
// });
