import React from "react";
import { render, screen } from "@testing-library/react";
import Result from "../components/Result";

test("test du composant Result", () => {
  // Rendu du composant Result avec une valeur fictive
  const value = 42;
  render(<Result value={value} />);

  // Vérification de l'affichage de la valeur
  const resultElement = screen.getByText(value.toString());
  expect(resultElement).toBeInTheDocument();
});
